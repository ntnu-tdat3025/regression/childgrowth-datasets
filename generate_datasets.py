import numpy as np

np.random.seed(0)


def generate_length_weight():
    data = np.genfromtxt('length_weight_z.csv', delimiter='\t', skip_header=1)

    min_ = np.min(data[:, 0])
    max_ = np.max(data[:, 0])

    outdata = []
    for i in range(0, 1000):
        element = np.round(np.random.uniform(min_, max_), decimals=1)

        row = data[np.argwhere(data[:, 0] == element)[0][0]]
        m = row[5]
        sd = row[6] - row[5]

        outdata.append([element, np.random.normal(m, sd)])

    np.savetxt('length_weight.csv', outdata, delimiter=',', header='length,weight')


def generate_age_head_circumference():
    data = np.genfromtxt('day_head_circumference_z.csv', delimiter='\t', skip_header=1)

    min_ = np.min(data[:, 0])
    max_ = np.max(data[:, 0])

    outdata = []
    for i in range(0, 1000):
        element = np.round(np.random.uniform(min_, max_), decimals=0)

        row = data[np.argwhere(data[:, 0] == element)[0][0]]
        m = row[5]
        sd = row[6] - row[5]

        outdata.append([element, np.random.normal(m, sd)])

    np.savetxt('day_head_circumference.csv', outdata, delimiter=',', header='day,head circumference')


def generate_day_length_weight():
    data_length = np.genfromtxt('day_length_z.csv', delimiter='\t', skip_header=1)
    data_weight = np.genfromtxt('day_weight_z.csv', delimiter='\t', skip_header=1)

    min_ = np.min(data_length[:, 0])  # same in data_length and data_weight
    max_ = np.max(data_length[:, 0])  # same in data_length and data_weight

    outdata = []
    for i in range(0, 1000):
        element = np.round(np.random.uniform(min_, max_), decimals=0)  # same in data_length and data_weight

        row_length = data_length[np.argwhere(data_length[:, 0] == element)[0][0]]
        m_length = row_length[5]
        sd_length = row_length[6] - row_length[5]

        row_weight = data_weight[np.argwhere(data_length[:, 0] == element)[0][0]]
        m_weight = row_weight[5]
        sd_weight = row_weight[6] - row_weight[5]

        outdata.append([element, np.random.normal(m_length, sd_length), np.random.normal(m_weight, sd_weight)])

    np.savetxt('day_length_weight.csv', outdata, delimiter=',', header='day,length,weight')


generate_length_weight()
generate_age_head_circumference()
generate_day_length_weight()
